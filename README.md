#Lemmings, a testing plugin for Grasshopper#

### Consists of 4 Components: ###

* data sender
* data receiver
* auto-in
* auto-out

The auto-in/auto-out components create a "harness" to automatically wire up the component that is being testing each time it is loaded.  

The data sender runs the test, streaming new data to each input at each solution recalculation.

The data receiver/s capture the results of the testing, concatenating everything and saving as a CSV file.