﻿using Grasshopper.Kernel;

namespace ghTester
{
    public class ghTesterInfo : GH_AssemblyInfo
    {
        public override string AssemblyName
        {
            get
            {
                return "lemmings";
            }
        }

        //Override here any more methods you see fit.
        //Start typing public override..., select a property and push Enter.
    }
}