﻿using System;
using System.Collections.Generic;
using Rhino.Geometry;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Parameters;
using Grasshopper.Kernel.Data;
using System.Linq;


namespace ghTester
{
    public class ghTestDriver : GH_Component, IGH_VariableParameterComponent
    {
        private System.Windows.Forms.Timer trampoline = new System.Windows.Forms.Timer();
        private GH_Document ghDocument;
        private int count;
        int dataLength = 0;

        private List<string> dataOut = new List<string>();

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ghTestDriver()
            : base("Driver", "dr",
                "automated testing for Grasshopper (driver)",
                "Params", "Testing")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Activate", "Activate", "a", GH_ParamAccess.item);
            pManager.AddTextParameter("Test Name", "Test Name", "n", GH_ParamAccess.item);
            pManager.AddTextParameter("Save Location", "Save Location", "F", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddTextParameter("Status", "Status", "s", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Boolean act = false;
            DA.GetData<Boolean>(0, ref act);
            string name = "";
            DA.GetData<string>(1, ref name);
            string folder = "";
            DA.GetData<string>(2, ref folder);

            string status = "inactive";
            if (act)
            {
                //get all the data
                List<List<double>> inputData = new List<List<double>>();
                
                List<double> testValues = new List<double>();
                for (int i = 3; i < Params.Input.Count; i++)
                {
                    List<double> tList = new List<double>();
                    DA.GetDataList<double>(i, tList);
                    inputData.Add(tList);
                    if (i == 3)
                    {
                        dataLength = tList.Count;
                    }
                    else
                    {
                        if (tList.Count < dataLength)
                        {
                            dataLength = tList.Count;
                        }
                    }
                    testValues.Add(0);
                }



                List<double> theValues = new List<double>();
                if (count < dataLength)
                {
                    ghDocument = this.OnPingDocument();
                    ghDocument.SolutionEnd += documentSolutionEnd;
                    for (int v = 0; v < testValues.Count; v++)
                    {
                        testValues[v] = inputData[v][count];
                    }
                    status = "testing: test "+count.ToString();
                }
                else
                {
                    count = -1;
                    status = TestUtilities.writeData(name, folder, dataOut/*Data*/);
                    dataOut.Clear();
                    dataLength = 0;
                }
                count++;
                //return theValues;
                DA.SetData(0, status);
                for (int i = 1; i < Params.Output.Count; i++)
                {
                    DA.SetData(i, testValues[i-1]);
                }

            }
        }

        private void exportData(GH_Document GrasshopperDocument)
        {
            List<string> grNames = new List<string>();
            if (count < dataLength)
            {
                List<Grasshopper.Kernel.GH_Component> groups = GrasshopperDocument
                .Objects.OfType<Grasshopper.Kernel.GH_Component>()
                .Where(gr => gr.Name.Equals("Sensor"))
                .ToList<Grasshopper.Kernel.GH_Component>();

                string columns = "";
                if (count == 1)
                {
                    foreach (Grasshopper.Kernel.GH_Component gr in groups)
                    {
                        if (gr.Params.Input[1] != null)
                        {
                            var rawData = gr.Params.Input[1].VolatileData;
                            rawData.Flatten(new GH_Path(0));
                            if (rawData.AllData(true) != null && rawData.AllData(true).Count() > 0)
                            {
                                columns += rawData.AllData(true).ElementAt(0).ToString()+",";
                            }
                        }
                    }
                    dataOut.Add(columns);
                }
                columns = "";
                foreach (Grasshopper.Kernel.GH_Component gr in groups)
                {
                    if (gr.Params.Input[0] != null)
                    {
                        var rawData = gr.Params.Input[0].VolatileData;
                        rawData.Flatten(new GH_Path(0));
                        if (rawData.AllData(true) != null && rawData.AllData(true).Count() > 0)
                        {
                            for (int d = 0; d < rawData.AllData(true).Count(); d++)
                            {
                                columns += rawData.AllData(true).ElementAt(d).ToString() + ",";
                            }
                        }
                    }
                }
                dataOut.Add(columns);
            }
        }

        //the solution loop is based on the "trampoline" code by Giulio Piacentino
        // giulio@mcneel.com
        private void documentSolutionEnd(object sender, GH_SolutionEventArgs e)
        {
            ghDocument.SolutionEnd -= documentSolutionEnd;

            //capture results from sensors
            exportData(ghDocument);

            // The trampoline gives time to stop the infinitly recursive chain from canvas,
            // as well as mantains the call stack fixed in length
            trampoline.Interval = 1;
            trampoline.Tick += trampolineTick;
            trampoline.Start();
        }
        private void trampolineTick(object sender, EventArgs e)
        {
            trampoline.Tick -= trampolineTick;
            trampoline.Stop();
            ghDocument.NewSolution(true);
        }


        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return ghTester.Properties.Resources.lemmings;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{ee347566-ba1d-4f6f-af81-ad2ccb7349c8}"); }
        }
        bool IGH_VariableParameterComponent.CanInsertParameter(GH_ParameterSide side, int index)
        {
            if (side == GH_ParameterSide.Input && index > 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool IGH_VariableParameterComponent.CanRemoveParameter(GH_ParameterSide side, int index)
        {
            //We can only remove from the input
            if (side == GH_ParameterSide.Input && Params.Input.Count > 3 && index > 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        IGH_Param IGH_VariableParameterComponent.CreateParameter(GH_ParameterSide side, int index)
        {
            string pName = GH_ComponentParamServer.InventUniqueNickname("ABCDEFGHIJKLMNOPQRSTUVWXYZ", Params.Input);
            Param_Number param = new Param_Number();
            param.Name = pName;
            param.NickName = param.Name;
            param.Description = "Param" + (Params.Input.Count + 1);
            param.Access = GH_ParamAccess.list;
            param.SetPersistentData(0);

            Param_Number outParam = new Param_Number();
            outParam.Name = pName;
            outParam.NickName = param.Name;
            outParam.Description = "Out Param" + (Params.Output.Count + 1);
            Params.RegisterOutputParam(outParam, index - 2);

            return param;
        }

        bool IGH_VariableParameterComponent.DestroyParameter(GH_ParameterSide side, int index)
        {
            //Nothing to do here by the moment
            Params.UnregisterOutputParameter(Params.Output[index-2]);
            return true;
        }

        void IGH_VariableParameterComponent.VariableParameterMaintenance()
        {
            //Nothing to do here by the moment
        }
    }
    public class ghTestSensor : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ghTestSensor()
            : base("Sensor", "sensor",
                "automated testing for Grasshopper (sensor)",
                "Params", "Testing")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("testData", "testData", "td", GH_ParamAccess.item);
            pManager.AddTextParameter("testName", "testName", "nm", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.AddNumberParameter("testDataOut", "testDataOut", "tdo", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //double data = 0;
            //DA.GetData<double>(0, ref data);
            //DA.SetData(0, data);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // You can add image files to your project resources and access them like this:
                //return Resources.IconForThisComponent;
                return ghTester.Properties.Resources.lemmings;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{3084E2BA-DF79-46E0-B96A-485D46EDE5D6}"); }
        }
    }
    public class ghAutoOut : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ghAutoOut()
            : base("AutoOut", "AutoOut",
                "automatically connects to specified component",
                "Params", "Testing")
        {
        }

        private GH_Document ghDocument;
        string inName = "";
        int inNum = 0;

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("auto in", "auto in", "auto in", GH_ParamAccess.item);
            pManager[0].Optional = true;
            pManager.AddGenericParameter("name", "name", "name", GH_ParamAccess.item);
            pManager.AddGenericParameter("number", "number", "number", GH_ParamAccess.item);
            pManager.AddBooleanParameter("act", "act", "act", GH_ParamAccess.item);

        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("out", "out", "out", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            object data = null;
            DA.GetData<object>(0, ref data);
            string name = "";
            DA.GetData<string>(1, ref inName);
            int num = 0;
            DA.GetData<int>(2, ref inNum);
            Boolean act = false;
            DA.GetData<Boolean>(3, ref act);
            if (act)
            {
                ghDocument = this.OnPingDocument();
                ghDocument.SolutionEnd += addWires;
            }
            DA.SetData(0, data);
        }
        private void addWires(object sender, GH_SolutionEventArgs e)
        {
            ghDocument.SolutionEnd -= addWires;

            //capture results from sensors
            updateInput(inName, inNum);
        }
        private void updateInput(string name, int num)
        {
            ghDocument = this.OnPingDocument();
            List<Grasshopper.Kernel.GH_Component> comps = ghDocument
            .Objects.OfType<Grasshopper.Kernel.GH_Component>()
            .Where(cp => cp.Name.Equals(name))
            .ToList<Grasshopper.Kernel.GH_Component>();

            this.Params.Input[0].RemoveAllSources();
            foreach (Grasshopper.Kernel.GH_Component cp in comps)
            {
                if (cp.Params.Output[num] != null)
                {
                    this.Params.Input[0].AddSource(cp.Params.Output[num]);
                }
            }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // You can add image files to your project resources and access them like this:
                //return Resources.IconForThisComponent;
                return ghTester.Properties.Resources.lemmings;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{6E0D8DD6-5FF2-4B7F-8F9B-798E15208C38}"); }
        }
    }
    public class ghAutoIn : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ghAutoIn()
            : base("AutoIn", "AutoIn",
                "automatically connects to specified component",
                "Params", "Testing")
        {
        }

        private GH_Document ghDocument;
        string inName = "";
        int inNum = 0;

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("in", "in", "in", GH_ParamAccess.item);
            pManager.AddGenericParameter("name", "name", "name", GH_ParamAccess.item);
            pManager.AddGenericParameter("number", "number", "number", GH_ParamAccess.item);
            pManager.AddBooleanParameter("act", "act", "act", GH_ParamAccess.item);

        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("auto out", "auto out", "auto out", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            object data = 0;
            DA.GetData<object>(0, ref data);
            DA.GetData<string>(1, ref inName);
            DA.GetData<int>(2, ref inNum);
            Boolean act = false;
            DA.GetData<Boolean>(3, ref act);
            if (act)
            {
                ghDocument = this.OnPingDocument();
                ghDocument.SolutionEnd += addWires;
            }
            DA.SetData(0, data);
        }
        private void addWires(object sender, GH_SolutionEventArgs e)
        {
            ghDocument.SolutionEnd -= addWires;

            //capture results from sensors
            updateOutput(inName, inNum);
        }
        private void updateOutput(string name, int num)
        {
            ghDocument = this.OnPingDocument();
            List<Grasshopper.Kernel.GH_Component> comps = ghDocument
            .Objects.OfType<Grasshopper.Kernel.GH_Component>()
            .Where(cp => cp.Name.Equals(name))
            .ToList<Grasshopper.Kernel.GH_Component>();

            
            foreach (Grasshopper.Kernel.GH_Component cp in comps)
            {
                if (cp.Params.Input[num] != null)
                {
                    cp.Params.Input[num].RemoveAllSources();
                    cp.Params.Input[num].AddSource(this.Params.Output[0]);
                }
            }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // You can add image files to your project resources and access them like this:
                //return Resources.IconForThisComponent;
                return ghTester.Properties.Resources.lemmings;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{826C29F1-9E00-4F01-A57E-24B795087967}"); }
        }
    }
}